#include "direct.h"

direct::direct()
	: QObject()
{

}
direct::direct(const direct &other)
{
	dirType = other.dirType;
}
direct::direct( dir &other)
{
	dirType = other;
}
direct::~direct()
{

}
void direct::operator = (const QString str)
{
	if (str == "in")
		dirType = in;
	else
	if (str == "inOut")
		dirType = inOut;
	else
	if (str == "out")
		dirType = out;
	else dirType = non;

}
void direct::operator=(const direct &other)
{
	dirType = other.dirType;
}
void direct::operator=(const dir &other)
{
	dirType = other;
}
bool direct::operator==(const dir &other) const
{
	return (dirType == other);
}

void direct::paint(QPainter *painter, QRectF rect) {

	switch (dirType) {
	case in:
	{
		int N = 3;
		for (int i = 0; i < N; i++) {
			painter->drawLine(QPointF(rect.x() + rect.width()*i / N,	rect.y() + 0), 
				QPointF(rect.x() + rect.width(),						rect.y() + rect.height()*(N - i) / N));
			if (i) {
				painter->drawLine(QPointF(rect.x() + 0,					rect.y() + rect.height()*i / N), 
					QPointF(rect.x() + rect.width()*(N - i) / N,		rect.y() + rect.height()));
			}
		}
	}
		break;
	case out:
	{
		int N = 3;
		for (int i = 0; i < N; i++) {
			painter->drawLine(QPointF(rect.x() + 0,				rect.y() + rect.height()*i / N),
				QPointF(rect.x() + rect.width()*i / N,			rect.y() + 0));

			painter->drawLine(QPointF(rect.x() + rect.width(),	rect.y() + rect.height()*i / N),
				QPointF(rect.x() + rect.width()*i / N,			rect.y() + rect.height()));

		}
	}
		break;
	case inOut:
	{
		int N = 3;
		for (int i = 0; i < N; i++) {
			painter->drawLine(QPointF(rect.x() + 0, rect.y() + rect.height()*i / N),
				QPointF(rect.x() + rect.width()*i / N, rect.y() + 0));

			painter->drawLine(QPointF(rect.x() + rect.width(), rect.y() + rect.height()*i / N),
				QPointF(rect.x() + rect.width()*i / N, rect.y() + rect.height()));

			painter->drawLine(QPointF(rect.x() + rect.width()*i / N, rect.y() + 0),
				QPointF(rect.x() + rect.width(), rect.y() + rect.height()*(N - i) / N));
			if (i) {
				painter->drawLine(QPointF(rect.x() + 0, rect.y() + rect.height()*i / N),
					QPointF(rect.x() + rect.width()*(N - i) / N, rect.y() + rect.height()));
			}

		}

	}
		break;
	default:
		break;
	}
}